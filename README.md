# multiqc Singularity container
### Package multiqc Version 1.13
Create aggregate bioinformatics analysis reports across many samples and tools
MultiQC is a tool to create a single report with interactive plots for multiple bioinformatics analyses across many samples.

Homepage:

https://multiqc.info
https://github.com/ewels/MultiQC

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
multiqc Version: 1.13<br>
Singularity container based on the recipe: Singularity.multiqc_v1.13.def

Local build:
```
sudo singularity build multiqc_v1.13.sif Singularity.multiqc_v1.13.def
```

Get image help:
```
singularity run-help multiqc_v1.13.sif
```

Default runscript: multiqc<br>
Usage:
```
./multiqc_v1.13.sif --help
```
or:
```
singularity exec multiqc_v1.13.sif multiqc --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull multiqc_v1.13.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/multiqc/multiqc:latest

```

